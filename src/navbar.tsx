import './navbar.scss';
import { Route, NavLink } from 'inferno-router';

import sticks           from '../assets/sticks.png';
import sticksActive     from '../assets/sticks-active.png';
import surdo            from '../assets/surdo.png';
import surdoActive      from '../assets/surdo-active.png';
import megaphon         from '../assets/megaphon.png';
import megaphonActive   from '../assets/megaphon-active.png';
import schote           from '../assets/schote.png';
import schoteActive     from '../assets/schote-active.png';
import brille           from '../assets/brille.png';
import brilleActive     from '../assets/brille-active.png';
import gaestebuch       from '../assets/gaestebuch.png';
import gaestebuchActive from '../assets/gaestebuch-active.png';
import presse           from '../assets/presse.png';
import presseActive     from '../assets/presse-active.png';

const items = [
  { label: "Auftritte",
    location: "/gigs",
    symbol: { active: sticksActive, inactive: sticks},
    order: 4,
  },
  { label: "Eindrücke",
    location: "/impressions",
    symbol: { active: brilleActive, inactive: brille},
    order: 3,
  },
  { label: "Mitmachen",
    location: "/join",
    symbol: { active: surdoActive, inactive: surdo},
    order: 2,
  },
  { label: "Über uns",
    location: "/about",
    symbol: { active: schoteActive, inactive: schote},
    order: 1,
  },
  { label: "Kontakt",
    location: "/contact",
    symbol: { active: megaphonActive, inactive: megaphon},
    order: 2,
  },
  { label: "Gästebuch",
    location: "/guestbook",
    symbol: { active: gaestebuchActive, inactive: gaestebuch},
    order: 3,
  },
  { label: "Presse",
    location: "/press",
    symbol: { active: presseActive, inactive: presse},
    order: 4,
  },
];

interface LinkSymbol {
  active: string;
  inactive: string;
}

interface LinkProps {
  symbol: LinkSymbol;
  label: string;
  location: string;
  order: number;
}

const Arrow = () => (
  <svg viewBox="0 0 2 2" preserveAspectRatio="none" class="arrow">
    <path d="M 0 0 l 2 0 l -1 2 -1 -2"/>
  </svg>
);

const Item = ({ symbol, label, location, order }: LinkProps) => (
  <div class={`wrapper anim-${order}`}>
    <NavLink to={location}>
      <Arrow/>
      <img class="active" src={symbol.active} />
      <img class="inactive" src={symbol.inactive} />
      <span>{label}</span>
    </NavLink>
  </div>
);

export const Navbar = () => (
  <div class="navbar">
    {items.map((i) => Item(i))}
  </div>
);
