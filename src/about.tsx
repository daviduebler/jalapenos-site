import { Link } from 'inferno-router';
import './about.scss';

export const About = () => (
  <div class="about-wrapper">
    <h2>
      Laut – groovy – bassig – strahlende Gesichter – bunt geschminkte Leute –
      tanzendes Publikum - abwechslungsreiches Repertoire – scharf und belebend wie
      feurige Jalapenos
    </h2>

    <p>
      Wir bringen Marktplätze, Industriehallen, Straßenzüge und Wirtshäuser zum
      Beben. Mit unseren gewaltigen Trommeln und dem Sound der mittlerweile 60
      Mitglieder unserer buntgemischten Gruppe, fangen nicht nur die
      Fensterscheiben an mit unserem Beat mitzuwackeln.
    </p>

    <p>
      Seit 2012 erarbeiten wir uns ein breites Repertoire an Stücken, bei dem
      manche so bewegungsreich sind, dass man einfach nur staunend zuschaut und
      der nächsten Überraschung entgegenfiebert, oder so schwungvoll, dass
      selbst das eingestaubte Tanzbein sich wieder geschmeidig dem Zug der Musik
      ergibt.
    </p>

    <p>
      Von Sechs-achtel über Samba-reagge, Funk, Maracatú, zu Mambo, Merengue und
      Batucada spielen wir alles, was groovt und Spaß macht.
    </p>

    <p>
      Kommt einfach mal zu einem unserer <Link to="/gigs">nächsten Auftritte</Link> und
      seht und staunt, wie selbst der letzte Tanzmuffel unweigerlich mit dem
      Kopf wippt.
    </p>

    <p>
      Oder bucht uns für euer nächstes Event! Anfragen nehmen wir <Link to="/contact">
      hier</Link> entgegen.
    </p>

    <video controls preload="auto">
        <source src="assets/media/orig.mp4" type="video/mp4" />
    </video>
  </div>

);
