import { connect } from 'inferno-redux';
import { Component } from 'inferno';
import { IGigListEntry } from './lib/gig-list';
import { Redirect } from 'inferno-router';
import logo from '../assets/logo.png';
import loader from '../assets/loader.png';
import './intro.scss';

const GigList = ({ children }: any) => (
  <ul class="gigList">
    {children}
  </ul>
);

const dateFormatOptions = {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
};

const GigListItem = (g: IGigListEntry) => (
  <li>
    <span class="date">
      {new Date(g.date).toLocaleDateString('de-DE', dateFormatOptions)}
    </span>
    <span class="location">{g.place}</span>
    <span class="title">{g.title}</span>
  </li>
);

interface BGVideoProps {
  loaded: boolean;
  onLoaded: () => void;
}

export const BackgroundVideo = ({ loaded, onLoaded }: BGVideoProps) => (
  <div class="background-video-wrapper">
    <img class="loader"
         style={`opacity: ${loaded ? 0 : 1}`}
         src={loader} />
    <video class="background"
           style={`opacity: ${loaded ? 1 : 0}`}
           volume="0.2"
           onCanPlayThrough={onLoaded}
           muted
           autoPlay>

      <source src="assets/media/intro.mp4" type="video/mp4" />
    </video>
  </div>
);

interface IntroProps {
}

interface IntroState {
  videoLoaded: boolean;
  shouldClose: boolean;
  isClosed: boolean;
}

export const Intro = connect(
  state => ({'nextGigs': state.data.calendar.next}),
  dispatch => ({})
)(class extends Component<IntroProps, IntroState> {
  protected state: IntroState;

  constructor() {
    super();
    this.state = {
      videoLoaded: false,
      shouldClose: false,
    };
  }

  public componentWillMount() {
    setTimeout(this.onVideoLoaded.bind(this), 5000);
  }

  private onAnimationEnd() {
    if (! this.state.shouldClose) return;
    this.setState({isClosed: true});
  }

  private onClick() {
    this.setState({shouldClose: true});
  }

  private onVideoLoaded() {
    if (this.state.videoLoaded) return;
    this.setState({videoLoaded: true});
  }

  public render({nextGigs}) {
    if (this.state.isClosed) {
      return <Redirect to="/about" />;
    }

    const overlay = (
      <div class="overlayWrapper" onClick={this.onClick.bind(this)}>
        <div class="spacer" />
        <img class="logo" src={logo}/>
        <div class="divider" />
        <div class="gigListWrapper">
          <GigList>
            { nextGigs.map(GigListItem).toJS() }
          </GigList>
        </div>
      </div>
    );

    return (
      <div class={`intro-wrapper ${this.state.shouldClose ? 'closing' : ''}`}
           onAnimationEnd={this.onAnimationEnd.bind(this)}>
        <BackgroundVideo loaded={this.state.videoLoaded}
                         onLoaded={this.onVideoLoaded.bind(this)}/>
        { this.state.videoLoaded ? overlay : '' }
      </div>
    );
  }
});
