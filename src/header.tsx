import logo from '../assets/logo-header.png';
import './header.scss';

export const Header = () => (
  <div class="header-container">
    <div class="background-wrapper" />
    <div class="logo-wrapper">
      <img class="logo" src={logo}/>
    </div>

    <svg class="line" viewBox="0 0 100 1" preserveAspectRatio="none">
      <line x1="0" y1="0.5" x2="100" y2="0.5"/>
    </svg>
  </div>
);
