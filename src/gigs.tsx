import { List } from 'immutable';
import { Component } from 'inferno';
import { connect } from 'inferno-redux';
import { monthTitle } from './lib/calendar';
import { IGigListEntry, IFlyerDescription } from './lib/gig-list';
import { ToolTip } from './tooltip';
import { SelectDay } from './lib/selectDay';
import kringel from '../assets/kringel.png';

import './gigs.scss';

interface CalendarDay {
  date: Date;
  entry?: IGigListEntry;
};

const dayMarker = (rotation: number) =>
  <img class="marker"
       style={`transform: rotate(${rotation}turn)`}
       src={kringel} />;

const rotationForDate = (date) =>
  ((date.getDate() + date.getMonth() + date.getDay()) % 30) / 30.0;

const Flyer = (flyer: IFlyerDescription) => {
  const ASSETS_PATH = './assets/gig-flyers';
  const thumb = `${ASSETS_PATH}/${flyer.thumb}`;
  const target = `${ASSETS_PATH}/${flyer.target}`;

  return (
    <a href={target} target="blank">
      <img class="flyerThumb" src={thumb}/>
    </a>
  );
}

const Gig = (entry: IGigListEntry) => (
  <div class="gig">
    <h3 class="title">{entry.title}</h3>
    {entry.flyer ? Flyer(entry.flyer) : ''}
    <span class="subtitle">{entry.place}</span>
    <span class="description">{entry.time}</span>
  </div>
);

interface DayProps {
  day: CalendarDay | undefined;
  selectedDay: Date | undefined;
  select: (Date) => any;
}

const Day = connect(
  state => ({selectedDay: state.getIn(['ui', 'selectedDay'])}),
  dispatch => ({select: (date) => dispatch(SelectDay.emit(date))})
)(({day, selectedDay, select}: DayProps) => (
  day.date
    ? <span class={`day ${day.entry ? 'marked' : ''}`}
            onClick={(ev) => {
              if (!day.entry) return;
              ev.stopPropagation();
              select(day.date);
            }}
      >
        {day.date.getDate()}
        {day.entry ? dayMarker(rotationForDate(day.date)) : ''}
        {selectedDay === day.date
          ? <ToolTip>{Gig(day.entry)}</ToolTip>
          : undefined
        }
      </span>
    : <span class="day"/>
));

interface WeekProps {
  days: List<CalendarDay>;
}

const Week = ({days}: WeekProps) => (
  <div class="week">
    {days.map(day => <Day day={day}/>).toJS()}
  </div>
);

interface MonthProps {
  weeks: List<List<CalendarDay>>;
}

const firstDayInMonth = (month: List<List<CalendarDay>>) =>
  month.first().filter(day => day.date !== undefined).first();

const Month = ({weeks}: MonthProps) => (
  <div class="month">
    <h2>{monthTitle(firstDayInMonth(weeks).date)}</h2>
    {weeks.map(days => (<Week days={days} />)).toJS()}
  </div>
);

interface YearProps {
  months: List<List<List<CalendarDay>>>;
  unselectDay: () => any;
}

const Year = connect(
  state => ({months: state.data.calendar.months}),
  dispatch => ({unselectDay: () => dispatch(SelectDay.emit(undefined))}),
)(
  ({months, unselectDay}: YearProps) => (
    <div class="year" onClick={() => unselectDay()}>
      <div class="months">
        {months.map(weeks => <Month weeks={weeks} />).toJS()}
      </div>
    </div>
  ));


const PreviousGig = ({date, place, title, comment}) => (
  <tr>
    <td>{new Date(date).toLocaleDateString('de-DE')}</td>
    <td>{place}</td>
    <td>{title}</td>
    <td>{comment}</td>
  </tr>
);


class PreviousYear extends Component {
  constructor(props) {
    super(props);
    this.state = {'expanded': false};
  }

  public render({year, gigs}, {expanded}) {
    return (
      <div class="year">
        {expanded
         ? (<h2 class="clickable"
                onClick={() => this.setState({'expanded': false})}>
              {year}
            </h2>)
         : (<h2 class="clickable"
                onClick={() => this.setState({'expanded': true})}>
              {year} ({gigs.length}...)
            </h2>)
        }

        <table>
          {expanded
          ? gigs.map(g => <PreviousGig date={g.date}
                                        place={g.place}
                                        title={g.title}
                                        comment={g.comment} />)
          : undefined}
        </table>
      </div>
    );
  }
}

const PreviousGigs = connect(
  state => ({previous: state.data.calendar.previous})
)(({previous}) => (
  <div>
    <h1>Vergangene Auftritte</h1>
    <div class="previous-wrapper">
      {previous.map(el => <PreviousYear year={el[0]} gigs={el[1]} />)}
    </div>
  </div>
));


export const Gigs = connect(
  state => ({ year: state.data.calendar.year })
)(({ year }) => (
  <div class="gigs">
    <h1>Auftritte {year}</h1>
    <Year/>
    <PreviousGigs/>
  </div>
));
