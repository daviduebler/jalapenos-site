import { render } from 'inferno';
import { Provider } from 'inferno-redux';
import { store } from './lib/store';
import { Page } from './page';
import './main.scss';

const container = document.getElementById('root');
render(<Provider store={store}><Page/></Provider>, container);
