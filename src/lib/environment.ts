export const ENVIRONMENT = {
  'apiUrl'                  : 'http://api.jalapenos.du.intern',
  'apiSendMessageUrl'       : 'http://api.jalapenos.du.intern/contact-messages',
  'apiFetchGuestbookEntries': 'http://api.jalapenos.du.intern/guestbook/entries',
  'apiSubmitGuestbookEntry' : 'http://api.jalapenos.du.intern/guestbook/entries',
  'apiAcceptGuestbookEntry' : (id) => 'http://api.jalapenos.du.intern/guestbook/entries/' + id,
  'apiFetchGigs'            : 'http://api.jalapenos.du.intern/gigs/entries',
};
