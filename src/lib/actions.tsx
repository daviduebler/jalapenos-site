import { IAppState } from './state';

type ActionID = string;

export interface ActionItem {
  type: ActionID;
  [key: string]: any;
}

abstract class IActionHandler {
  abstract get type(): string;
  abstract reduce(state: IAppState, action: ActionItem): IAppState;
}

export function ActionHandler<Payload>(
  actionType: string,
  reduceFn: (state: IAppState, action: ActionItem) => IAppState,
  decorateFn: (action: ActionItem, payload: Payload) => ActionItem =
    (a, p) => ({ ...a, payload: p }),
)
{
  return class extends IActionHandler {
    public get type(): string { return actionType; }

    public static emit(payload: Payload) {
      return decorateFn({ type: actionType }, payload);
    }

    public reduce(state: IAppState, action: ActionItem): IAppState {
      return reduceFn(state, action);
    }
  };
}

export class ActionRegistry {
  private handlers: {[type: string]: IActionHandler} = {};

  public register(handler: IActionHandler): void {
    this.handlers[handler.type] = handler;
  }

  public lookup(type: string): IActionHandler | undefined {
    return this.handlers[type];
  }
}
