export const previousGigsList = [
  {
    "place": "K\u00f6ln",
    "date": "2018-11-11",
    "comment": "Einl\u00e4uten des K\u00f6lner Karnevals mit vielen anderen Gruppen im fernen K\u00f6ln -K\u00f6lle Alaaf!!",
    "title": "K\u00f6lner Karneval"
  },
  {
    "place": "Amberg",
    "date": "2018-10-12",
    "comment": "",
    "title": "Demozug \u201eAmberg ist bunt\u201c"
  },
  {
    "place": "Weiden",
    "date": "2018-09-22",
    "comment": "",
    "title": "HAMM Baskets"
  },
  {
    "place": "Amberg",
    "date": "2018-09-07",
    "comment": "Grandiose Jubil\u00e4umsfeier eines der gr\u00f6\u00dften Unternehmen in Amberg \u2013 Danke den Organisatoren!",
    "title": "Siemens Firmenfest "
  },
  {
    "place": "N\u00fcrnberg",
    "date": "2018-07-28",
    "comment": "",
    "title": "N\u00fcrnberger Bardentreffen"
  },
  {
    "place": "Amberg",
    "date": "2018-07-21",
    "comment": "",
    "title": "Marine-Kameradschaftsabend"
  },
  {
    "place": "Weiden",
    "date": "2018-07-20",
    "comment": "...und zwar von den Jalapenos!\u2026",
    "title": "Weiden tr\u00e4umt"
  },
  {
    "place": "Hahnbach",
    "date": "2018-07-08",
    "comment": "",
    "title": "Internationales Musikfest"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2018-07-06",
    "comment": "",
    "title": "Kunst & Rotlicht"
  },
  {
    "place": "Wernberg K\u00f6blitz",
    "date": "2018-06-29",
    "comment": "Ein sch\u00f6ner Event mit vielen tanzbegeistern Besuchern und super Stimmung",
    "title": "50-j\u00e4hriges Jubil\u00e4um von Dr. L\u00f6w"
  },
  {
    "place": "Kallm\u00fcnz",
    "date": "2018-06-16",
    "comment": "",
    "title": "Flammende Visionen"
  },
  {
    "place": "Schirmitz",
    "date": "2018-06-09",
    "comment": "Immer wieder sch\u00f6n!",
    "title": "Dorffest"
  },
  {
    "place": "Weiden",
    "date": "2018-05-19",
    "comment": "Die zum Teil \"feuchte\" und kulinarische Fanmeile beim Foodtruck-Festival haben wir gerne gerockt, eine tolle Attraktion",
    "title": "Foodtruck-Festival"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2018-05-09",
    "comment": "Wieder haben wir gerne die \u00fcber 7000 L\u00e4ufer zu Bestleistungen angefeuert und unsere treuen Fans unterhalten!",
    "title": "NOFI-Lauf"
  },
  {
    "place": "Amberg",
    "date": "2018-04-29",
    "comment": "Immer wieder gerne unterhalten wir heimisches Publikum!",
    "title": "Entenrennen"
  },
  {
    "place": "Amberg",
    "date": "2017-02-23",
    "comment": "Eine tolle Aktion in Amberg, sch\u00f6n, dass wir dabei sein durften!",
    "title": "One billion rising"
  },
  {
    "place": "Weiden",
    "date": "2017-10-15",
    "comment": "",
    "title": "Weltb\u00fcrgerfest"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2017-09-22",
    "comment": "Erstmals zu Gast und sch\u00f6n war\u2019s! Gerne wieder!",
    "title": "Kulturnacht"
  },
  {
    "place": "Fichtelbrunn",
    "date": "2017-09-17",
    "comment": "Ein tolles Event mit einem noch besseren Publikum!",
    "title": "Veteranen-/Oldtimer-Treffen"
  },
  {
    "place": "Weiden",
    "date": "2017-07-21",
    "comment": "Erneut ein begeistertes Publikum in Weiden, es ist immer ein Genuss in Weiden aufzutreten",
    "title": "Weiden tr\u00e4umt"
  },
  {
    "place": "Coburg",
    "date": "2017-07-14",
    "comment": "Erstmals seit unserem Bestehen durften wir uns beim legend\u00e4ren 3-t\u00e4gigen Samba-Festival pr\u00e4sentieren. Nicht nur f\u00fcr uns Sambistas war das ein unfassbares Erlebnis - auch haben sehr viele positive Eindr\u00fccke hinterlassen und viel Lob erhalten!",
    "title": "Coburger Samba-Festival"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2017-06-24",
    "comment": "Immer ein Erlebnis!",
    "title": "Altstadtfest"
  },
  {
    "place": "Schirmitz",
    "date": "2017-06-10",
    "comment": "Erneut durften wir das tolle Dorffest rocken!",
    "title": "Dorffest"
  },
  {
    "place": "Amberg",
    "date": "2017-05-28",
    "comment": "Das tolle Event des Luftmuseum haben wir bei karibischen Temperaturen gerne begleitet",
    "title": "Luftmuseumstag"
  },
  {
    "place": "Kemnath",
    "date": "2017-05-24",
    "comment": "Gerne haben wir das Sportevent unserer Region unterst\u00fctzt!",
    "title": "NOFI-Lauf"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2017-05-20",
    "comment": "",
    "title": "Fr\u00fchlingsfest-Umzug"
  },
  {
    "place": "Engelsh\u00fctt",
    "date": "2017-04-29",
    "comment": "",
    "title": "Hexennacht"
  },
  {
    "place": "Amberg",
    "date": "2017-04-23",
    "comment": "",
    "title": "Entenrennen/Verkaufsoffener Sonntag"
  },
  {
    "place": "Herzogenaurach",
    "date": "2017-04-01",
    "comment": "",
    "title": "Gewerbegebiets-Jubil\u00e4um"
  },
  {
    "place": "Vohenstrau\u00df",
    "date": "2017-01-29",
    "comment": "",
    "title": "Ostbayerischer Faschingsumzug"
  },
  {
    "place": "Amberg",
    "date": "2016-12-07",
    "comment": "Wir durften die perfekt organisierte Firmen-Jahresabschlussfeier mit unserer Show aufwerten, Danke Heike!",
    "title": "Firmen-Jahresabschlussfeier"
  },
  {
    "place": "Regensburg",
    "date": "2016-11-19",
    "comment": "Wir waren bei dem rauschenden Fest und einer Nacht voller Trommeln dabei und bedanken uns ganz herzlich bei den befreundeten Sambistas",
    "title": "20-Jahr-Feier von Sarar\u00e1 aus Regensburg"
  },
  {
    "place": "Amberg",
    "date": "2016-09-09",
    "comment": "",
    "title": "Luftnacht"
  },
  {
    "place": "Rietberg",
    "date": "2016-08-19",
    "comment": "Erstmalig konnten wir auf einem mehrt\u00e4gigen Samba-Festival auftreten. Es hat uns riesig viel Spa\u00df gemacht und wir haben bei den Profis Eindruck hinterlassen k\u00f6nnen (-:",
    "title": " Samba-Festival Cultura do Brasil"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2016-07-31",
    "comment": "",
    "title": "BR-Radltour"
  },
  {
    "place": "Waidhaus",
    "date": "2016-07-24",
    "comment": "",
    "title": " B\u00fcrgerfest"
  },
  {
    "place": "Weiden",
    "date": "2016-07-22",
    "comment": "Ein wirklich traumhafter Abend mit einem traumhaften Publikum. Wir kommen wieder.",
    "title": "Weiden tr\u00e4umt"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2016-07-01",
    "comment": "Sehr sch\u00f6n war es wieder bei euch!",
    "title": "Kunst & Rotlicht"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2016-06-25",
    "comment": "Ein absolut grandioses Publikum hat uns bei den Auftritten begleitet. Ganz herzlichen Dank an die Veranstalter und unsere Fans!",
    "title": "Altstadtfest"
  },
  {
    "place": "Schirmitz",
    "date": "2016-06-11",
    "comment": "",
    "title": "Dorffest"
  },
  {
    "place": "N\u00fcrnberg",
    "date": "2016-05-28",
    "comment": "",
    "title": "Auftritt in der Fu\u00dfg\u00e4ngerzone"
  },
  {
    "place": "Weiden",
    "date": "2016-05-04",
    "comment": "",
    "title": "NOFI-Lauf"
  },
  {
    "place": "Engelsh\u00fctt",
    "date": "2016-04-30",
    "comment": "",
    "title": "Hexennacht"
  },
  {
    "place": "Weiden",
    "date": "2016-04-10",
    "comment": "",
    "title": "Contest \"Dance your Style\""
  },
  {
    "place": "Schnaittenbach",
    "date": "2016-03-18",
    "comment": "",
    "title": "Private Geburtstagsfeier"
  },
  {
    "place": "Kemnath",
    "date": "2015-12-11",
    "comment": "Eine tolle Weihnachtsfeier haben wir berockt - Vielen Dank f\u00fcr den Applaus und die Feier im Anschluss!\u00a0",
    "title": "Weihnachtsfeier der Firma Rogers"
  },
  {
    "place": "Altenburg",
    "date": "2015-10-03",
    "comment": "mit  Aipal\u00e9 und Como Vento und anderen Amigos do Samba",
    "title": "Samba-Nacht"
  },
  {
    "place": "Pilsen",
    "date": "2015-09-12",
    "comment": "",
    "title": "Welt\u00e4rztekongress"
  },
  {
    "place": "Grafenw\u00f6hr",
    "date": "2015-08-01",
    "comment": "",
    "title": "Dt.-Amerikanisches Volksfest"
  },
  {
    "place": "Hirschau",
    "date": "2015-07-25",
    "comment": "",
    "title": "Gruppen-Sommerfest auf den Anh\u00f6hen des Monte Kaolino\u00a0"
  },
  {
    "place": "Weiden",
    "date": "2015-07-24",
    "comment": "Ein begeistertes Publikum animierte uns zu Glanzleistungen - Danke Weiden!\u00a0",
    "title": "Weiden tr\u00e4umt"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2015-06-27",
    "comment": "",
    "title": "Altstadtfest"
  },
  {
    "place": "Amberg",
    "date": "2015-06-14",
    "comment": "",
    "title": "Altstadtfest"
  },
  {
    "place": "Pilsen",
    "date": "2015-06-13",
    "comment": "",
    "title": "Historisches Fest und 720-Jahrfeier der Stadt Pilsen"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2015-05-13",
    "comment": "",
    "title": "NOFI-Lauf"
  },
  {
    "place": "Engelsh\u00fctt",
    "date": "2015-04-30",
    "comment": "",
    "title": "Hexennacht"
  },
  {
    "place": "Amberg",
    "date": "2015-04-18",
    "comment": "",
    "title": "Benefiz-Veranstaltung zugunsten der Fl\u00fcchtlingsarbeit"
  },
  {
    "place": "Schnaittenbach",
    "date": "2015-02-15",
    "comment": "",
    "title": "Faschingszug"
  },
  {
    "place": "Amberg",
    "date": "2015-02-14",
    "comment": "",
    "title": "Benefiz-Veranstaltung \"ONE BILLION RISING\u201d"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2015-02-01",
    "comment": "",
    "title": "Gro\u00dfer Ostbayernumzug"
  },
  {
    "place": "Amberg",
    "date": "2014-10-12",
    "comment": "",
    "title": "Saison-Er\u00f6ffnungsspiel des ERSC Amberg\u00a0"
  },
  {
    "place": "Amberg",
    "date": "2014-09-15",
    "comment": "",
    "title": "Private Geburtstagsfeier"
  },
  {
    "place": "Amberg",
    "date": "2014-09-12",
    "comment": "",
    "title": "Luftnacht"
  },
  {
    "place": "Lam",
    "date": "2014-08-23",
    "comment": "",
    "title": "Marktplatzfest KuliNariMusiSumma"
  },
  {
    "place": "Grafenw\u00f6hr",
    "date": "2014-08-03",
    "comment": "",
    "title": "Deutsch-Amerikanisches Volksfest"
  },
  {
    "place": "Schmidm\u00fchlen",
    "date": "2014-08-02",
    "comment": "",
    "title": "Marktfest"
  },
  {
    "place": "Amberg",
    "date": "2014-07-26",
    "comment": "",
    "title": "Privates Sommerfest"
  },
  {
    "place": "Amberg",
    "date": "2014-07-20",
    "comment": "Was f\u00fcr ein tolles und hei\u00dfes Wochenende in unserer Heimatstadt! Wir haben bei \u00e4u\u00dferst hei\u00dfen Temperaturen alles gegeben und gaaanz viel Begeisterung f\u00fcr unsere Musik erhalten. Danke Amberg! \u00a0",
    "title": "Altstadtfest"
  },
  {
    "place": "Weiden",
    "date": "2014-07-18",
    "comment": "",
    "title": "Weiden tr\u00e4umt"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2014-07-04",
    "comment": "",
    "title": "Kunst und Rotlicht"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2014-06-28",
    "comment": "",
    "title": "Altstadtfest"
  },
  {
    "place": "Weiden",
    "date": "2014-06-19",
    "comment": "",
    "title": "Sommerfest der Pizzeria \"Kleine Freiheit\""
  },
  {
    "place": "Hirschau",
    "date": "2014-06-01",
    "comment": "",
    "title": "Benefizveranstaltung f\u00fcr die Lebenshilfe Amberg"
  },
  {
    "place": "Tirschenreuth",
    "date": "2014-05-28",
    "comment": "F\u00fcr unser anfeuerndes Trommeln haben wir sehr viele begeisterte R\u00fcckmeldungen erhalten",
    "title": "NOFI-Lauf"
  },
  {
    "place": "K\u00fcmmersbruck",
    "date": "2014-05-25",
    "comment": "",
    "title": "Festumzug zum 900j\u00e4hrigen Bestehen von K\u00fcmmersbruck"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2014-05-24",
    "comment": "",
    "title": "Umzug zum Fr\u00fchlingsfest"
  },
  {
    "place": "Schwarzenfeld",
    "date": "2014-05-23",
    "comment": "Ein wirklich toller Abend mit grandiosem Publikum und sehr netten Gastgebern, allen voran Danke an Herrn Wittleben und Frau Gruber und allen weiteren Mitarbeitern der Apotheke St. Nikolaus!",
    "title": "XXL-Einkaufsabend mit Weltreise"
  },
  {
    "place": "Theuern",
    "date": "2014-03-14",
    "comment": "",
    "title": "Sportler-Ehrung Landkreis AS in Schloss Theuern"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2014-03-03",
    "comment": "",
    "title": "Gro\u00dfer Faschingsumzug"
  },
  {
    "place": "K\u00fcmmersbruck",
    "date": "2014-02-16",
    "comment": "Erfolgreich konnten wir die Regenwolken wegtrommeln, so dass es ein vergn\u00fcglicher und trockener Faschingszug - erstmals mit s\u00fcdamerikanischen Ryhthmen - werden konnte.",
    "title": "Jubil\u00e4ums-Faschingsumzug"
  },
  {
    "place": "Haselm\u00fchl",
    "date": "2014-02-15",
    "comment": "Ein sehr unterhaltsamer und durch uns bereicherter Faschingsball",
    "title": "Faschingsball Narhalla"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2014-02-08",
    "comment": "Einer gut gelaunten und fr\u00f6hlichen Faschingsgesellschaft konnten wir erneut eine tolle Show bieten. Hat uns mega Spa\u00df gemacht!",
    "title": "Faschingssitzung"
  },
  {
    "place": "Theuern",
    "date": "2014-01-10",
    "comment": "Eine beeindruckende Faschingsveranstaltung von Narhalla Haselm\u00fchl-Amberg-K\u00fcmmersbruck - mit 2 Auftritten haben wir mit unseren Samba-Rhythmen zur tollen Show beigetragen.",
    "title": "Inthronisierung in Schloss Theuern"
  },
  {
    "place": "Amberg",
    "date": "2013-12-22",
    "comment": "Eine etwas andere Kullisse in einer imposanten Firmenanlage und wir hoffen wir haben Gefallen hinterlassen.",
    "title": "Weihnachtsfeier Luitpoldh\u00fctte"
  },
  {
    "place": "Amberg",
    "date": "2013-12-19",
    "comment": "",
    "title": "Gruppen-Weihnachtsfeier"
  },
  {
    "place": "Ursensollen",
    "date": "2013-11-29",
    "comment": "Die G\u00e4ste und wir hatten viel Spa\u00df mit Dinnerdrumming und eine tolle Show mit Jalape\u00f1os Percussion. Wir bedanken uns f\u00fcr einen sch\u00f6nen Abend",
    "title": "Weihnachtsfeier SheepWorld"
  },
  {
    "place": "Weiden",
    "date": "2013-11-08",
    "comment": "3,5 Stunden Marathon-Auftritte vor begeistertem Publikum, die mit uns durch die Nacht gezogen sind. Und viele sch\u00f6ne Attraktionen; eine tolle Aktion von ProWeiden. Danke f\u00fcr das Engagement - wir hatten mindestens so viel Spa\u00df wie Ihre G\u00e4ste!",
    "title": "Kunstgenuss bis Mitternacht"
  },
  {
    "place": "Amberg",
    "date": "2013-10-05",
    "comment": "Danke f\u00fcr die Einladung, es war ein wundersch\u00f6nes Fest!",
    "title": "Private Geburtstagsfeier"
  },
  {
    "place": "Raigering",
    "date": "2013-09-26",
    "comment": "Wir haben uns gefreut f\u00fcr Euch spielen zu d\u00fcrfen!\u00a0",
    "title": "Stra\u00dfenfest"
  },
  {
    "place": "Schnaittenbach",
    "date": "2013-09-21",
    "comment": "Danke f\u00fcr die Einladung f\u00fcr das tolle Fest!",
    "title": "Private Geburtstagsfeier"
  },
  {
    "place": "Nittenau",
    "date": "2013-08-24",
    "comment": "Ein wirklich beeindruckendes Stra\u00dfenfest bei s\u00fcdl\u00e4ndischen Temperaturen mit vielen gut gelaunten G\u00e4sten. Wir durften viel Lob und Begeisterung erfahren. Danke Werbegemeinschaft Nittenau!",
    "title": "Kulinarischer Abend"
  },
  {
    "place": "Tirschenreuth",
    "date": "2013-08-10",
    "comment": "Als besonderes Highlight durften wir die G\u00e4ste des Amerikanischen Wochenendes unterhalten - und es hat richtig Spa\u00df gemacht!",
    "title": "Gartenschau"
  },
  {
    "place": "Neustadt Waldnaab",
    "date": "2013-07-05",
    "comment": "Wir fanden eine tolle Atmosph\u00e4re und grandioses Publikum vor!",
    "title": "Stadtfest \"Kunst + Rotlicht\""
  },
  {
    "place": "Amberg",
    "date": "2013-07-04",
    "comment": "Ein St\u00e4ndchen f\u00fcr die amerikanischen Gastsch\u00fcler des MRG!",
    "title": "Spontaner Begr\u00fc\u00dfungsauftritt"
  },
  {
    "place": "Sulzbach-Rosenberg",
    "date": "2013-06-29",
    "comment": "Trotz widriger Wetterlage tolles Publikum angezogen, wir sind n\u00e4chstes Jahr wieder dabei!",
    "title": "Altstadtfest"
  },
  {
    "place": "Amberg",
    "date": "2013-06-27",
    "comment": "Auch von dieser Stelle noch einmal herzlichen Gl\u00fcckwunsch!",
    "title": "Private Geburtstagsfeier"
  },
  {
    "place": "Amberg",
    "date": "2013-06-15",
    "comment": "Wir konnten mit unseren 7 Auftritten an zwei Tagen das Publikum restlos begeistern! Danke an die vielen neu gewonnenen Fans.",
    "title": "Altstadtfest"
  },
  {
    "place": "Amberg",
    "date": "2013-04-20",
    "comment": "Wir waren Special Guests bei Cocoz Locoz Entertainment und hatten viel Spa\u00df und konnten das Publikum begeistern",
    "title": "Flash Back Graf \"La Fiesta meets Samba\""
  },
  {
    "place": "Amberg",
    "date": "2013-04-20",
    "comment": "Auch an dieser Stelle die besten Gl\u00fcckw\u00fcnsche und vielen Dank f\u00fcr das Engagement - Ihr ward ein tolles Publikum!",
    "title": "Private Geburtstagsfeier"
  },
  {
    "place": "Weiden",
    "date": "2012-07-08",
    "comment": "Vielen Dank an das Organisationsteam in Ullersricht und an Gabi ",
    "title": "B\u00fcrgerfest"
  },
  {
    "place": "Eschenbach (Opf.)",
    "date": "2012-07-29",
    "comment": "Vielen Dank an die Stadt Eschenbach, B\u00fcrgermeister Herr Lehr, Frau Wohlrab und Frau N\u00fcrnberger sowie das tolle Publikum!",
    "title": "B\u00fcrgerfest"
  },
  {
    "place": "Grafenw\u00f6hr",
    "date": "2012-08-04",
    "comment": "Ihr ward ein tolles Publikum; you have been a great audience!",
    "title": "Deutsch-Amerikanisches Volksfest"
  },
  {
    "place": "Amberg",
    "date": "2012-11-14",
    "comment": "Ein toller Abend und ein tolles Publikum - Danke an die Veranstalter!",
    "title": "Percussion-Night im Club Habana"
  }
];
