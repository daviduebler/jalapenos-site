import { setIn } from 'immutable';
import { ActionHandler } from './actions';
import { Page } from './page';

export const PageSwapRequest = ActionHandler<Page>(
  'page-swap-request',
  (s, a) => {
    const s1 = setIn(s, ['ui', 'pageSelection', 'requestedPage'], a.payload);

    return s.ui.pageSelection.currentPage === Page.Unspecified
         ? setIn(s1, ['ui', 'pageSelection', 'currentPage'], a.payload)
         : s1;
  });

export const PageSwap = ActionHandler<Page>(
  'page-swap',
  (s, a) => setIn(s, ['ui', 'pageSelection', 'currentPage'], a.payload)
);
