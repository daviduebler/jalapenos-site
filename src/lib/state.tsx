import { List, Record } from 'immutable';
import { Page } from './page';
import { getGigCalendar } from './gig-list';

const currentYear = (new Date()).getFullYear();

const CalendarState = Record({
  year: currentYear,
  months: getGigCalendar([], currentYear),
  previous: List(),
  next: List(),
});

const GuestbookState = Record({
  guestBookEntries: List()
});

const DataState = Record({
  calendar: CalendarState(),
  guestbook: GuestbookState(),
});

const PageSelection = Record({
  currentPage: Page.Unspecified,
  requestedPage: Page.Unspecified,
});

const GuestbookUiState = Record({
  entriesVisible: 10,
  entriesStep: 10,
});

const UiState = Record({
  pageSelection: PageSelection(),
  selectedDay: undefined,
  guestbook: GuestbookUiState(),
});

const AppState = Record({
  data: DataState(),
  ui: UiState()
});

export const initState = () => AppState();
