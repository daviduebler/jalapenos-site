import { List, Record, Map, setIn } from 'immutable';
import { calendarForYear } from './calendar';
import { ActionHandler } from './actions';
import { ENVIRONMENT } from './environment';

export interface IFlyerDescription {
  thumb: string;
  target: string;
}

const FlyerDescription = Record<IFlyerDescription>({
  thumb: 'missing',
  target: 'missing',
});

export interface IGigListEntry {
  date: string;
  time: string;
  place: string;
  title: string;
  flyer?: IFlyerDescription;
}

const GigListEntry = Record<IGigListEntry>({
  date: '01.01.1970',
  time: '00:00',
  place: '-',
  title: '-',
  flyer: undefined,
});

const PreviousGig = Record({
  date: '',
  place: '',
  title: '',
  comment: '',
});

const formatDate = (d: Date) => {
  return `${d.getFullYear()}-${d.getMonth()}-${d.getDate()}`;
};

const makeGigByDateWrapper = (currentGigsList) => {
  const gigs = List(List(currentGigsList).map(g => GigListEntry(g)));

  const gigMap: Map<string, IGigListEntry> = gigs.reduce(
    (acc: Map<string, IGigListEntry>, el: IGigListEntry) =>
      acc.set(formatDate(new Date(el.date)), el),
    Map());

  return (date: Date | undefined) => date && gigMap.get(formatDate(date));
}

export const getGigCalendar = (currentGigsList, currentYear) => {
  const gigByDate = makeGigByDateWrapper(currentGigsList);

  return (calendarForYear(currentYear)
          .map(m => m.map(
            w => w.map(
              d => ({date: d, entry: gigByDate(d)})))));
};

export const getPreviousGigs = (previousGigsList) => {
  const previous = List(previousGigsList).map(g => PreviousGig(g));

  const year = d => (new Date(d)).getFullYear();

  return previous.reduce(
    (acc, el) => acc.update(year(el.date),
                            List(),
                            v => v.push(el)),
    Map())
    .entrySeq()
    .sort((a, b) => a[0] < b[0])
    .toJS();
}

export const getNextGigs = (currentGigsList) => {
  const gigs = List(List(currentGigsList).map(g => GigListEntry(g)));
  const now = new Date();
  return gigs.filter((el: IGigListEntry) => new Date(el.date) >= now).take(3);
};

export const SetCurrentGigs = ActionHandler<Array<any>>(
  'set-current-gigs',
  (s, a) => setIn(s,
                  ['data', 'calendar', 'months'],
                  getGigCalendar(a.payload, s.data.calendar.year))
);

export const SetNextGigs = ActionHandler<Array<any>>(
  'set-next-gigs',
  (s, a) => setIn(s,
                  ['data', 'calendar', 'next'],
                  getNextGigs(a.payload))
);

export const SetPreviousGigs = ActionHandler<Array<any>>(
  'set-previous-gigs',
  (s, a) => setIn(s,
                  ['data', 'calendar', 'previous'],
                  getPreviousGigs(a.payload))
);

export const fetchGigs = async () => {
  const resp = await fetch(ENVIRONMENT.apiFetchGigs);
  const result = await resp.json();
  return result.entries;
};
