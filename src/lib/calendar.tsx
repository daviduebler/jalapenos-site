import { List, Range, Repeat } from 'immutable';

export type Week = List<Date>;
export type Month = List<Week>;
export type Year = List<Month>;

const MONTH_TITLES = List.of(
  'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
  'Juli', 'August', 'September', 'Oktober', 'November',
  'Dezember'
);

const months = (year: number): List<List<Date>> => {
  function* days(month: number): Iterable<Date> {
    let cur = new Date(year, month, 1);

    while (cur.getMonth() === month) {
      yield new Date(cur.getTime());
      cur.setDate(cur.getDate() + 1);
    }
  }

  return Range(0, 12).map(m => List(days(m))).toList();
};

const weeks = (month: List<Date>): List<Week> => {
  return month.reduce((acc: List<List<Date>>, el: Date) => {
    const weekDay = el.getDay();

    if (weekDay === 1) {
      return acc.push(List.of(el));
    } else {
      return acc.update(-1, ds => ds.push(el));
    }
  }, List.of(List()))
  .filter(el => el.size > 0);
};

const padWeek = (week: Week): List<Date | undefined> => {
  const padding = Repeat(undefined, 7 - week.size).toList();

  return week.first().getDate() === 1
       ? padding.concat(week)
       : week.concat(padding);
};

export const calendarForYear = (year: number): Year =>
  months(year).map(m => weeks(m).map(padWeek));

export const monthTitle = (date: Date) => MONTH_TITLES.get(date.getMonth());
