import { setIn } from 'immutable';
import { ActionHandler } from './actions';

export const SelectDay = ActionHandler<Date>(
  'select-day',
  (s, a) => setIn(s, ['ui', 'selectedDay'], a.payload)
);
