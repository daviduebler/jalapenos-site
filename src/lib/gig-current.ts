export const currentGigsList = [
  { date: '2019-02-24',
    time: '12:00',
    place: 'Amberg - Innenstadt',
    title: 'Ostbayrischer Faschingsumzug'
  },
  { date: '2019-04-28',
    time: '',
    place: 'Amberg - Luftmuseeum',
    title: 'Entenrennen Amberg'
  },
  { date: '2019-06-01',
    time: '20:00',
    place: 'Schirmitz',
    title: 'Dorffest Schirmitz'
  },
  { date: '2019-06-28',
    time: '',
    place: 'Regensburg',
    title: 'Bürgerfest Regensburg'
  },
  { date: '2019-07-14',
    time: '',
    place: 'Schwandorf - Innenstadt',
    title: 'Stadtfest Schwandorf'
  },
  { date: '2019-07-26',
    time: '',
    place: 'Weiden - Innenstadt',
    title: 'Weiden träumt'
  },
  { date: '2019-07-27',
    time: '',
    place: 'Monte Kaolino, Hirschau',
    title: 'Beachparty am Monte'
  },
  { date: '2019-08-30',
    time: '',
    place: 'Bad Wildungen',
    title: 'Sambafestival Bad Wildungen'
  },

  // { date: '',
  //   time: '',
  //   place: '',
  //   title: ''
  // },
];
