import { ActionHandler } from './actions';
import { List, setIn } from 'immutable';

import { IGuestbookEntry } from './state';
import { ENVIRONMENT } from './environment';

export const fetchEntries = async () => {
  const resp = await fetch(ENVIRONMENT.apiFetchGuestbookEntries);
  const result = await resp.json();
  return result.entries;
};

export const submitEntry = async (txtMessage: string, actions: any) =>
{
  actions.sending();

  try {
    await fetch(ENVIRONMENT.apiSubmitGuestbookEntry, {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        'message': txtMessage
      }),
    });

    actions.success();
  } catch(_) {
    actions.failed();
  }
};

export const acceptEntry = async (id: string, pin: string) =>
{
  await fetch(ENVIRONMENT.apiAcceptGuestbookEntry(id), {
    method: "PATCH",
    mode: "cors",
    cache: "no-cache",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      'pin': pin
    }),
  });
}

export const SetGuestBookEntries = ActionHandler<Array<IGuestbookEntry>>(
  'set-guestbook-entries',
  (s, a) => setIn(s,
                  ['data', 'guestbook', 'guestBookEntries'],
                  List<IGuestbookEntry>(a.payload))
);

export const SetVisibleEntries = ActionHandler<number>(
  'set-visible-entries',
  (s, a) => setIn(s,
                  ['ui', 'guestbook', 'entriesVisible'],
                  a.payload)
);
