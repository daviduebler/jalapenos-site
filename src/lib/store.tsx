import { createStore } from 'redux';
import { ActionItem, ActionRegistry } from './actions';
import { IAppState, initState } from './state';

import { PageSwapRequest, PageSwap } from './pageSwap';
import { SelectDay } from './selectDay';

import * as guestbook from './guestbook';
import * as gigList from './gig-list';

//-------------------------------------------------------------------------------
const actionRegistry = new ActionRegistry();
actionRegistry.register(new PageSwapRequest());
actionRegistry.register(new PageSwap());

actionRegistry.register(new SelectDay());

actionRegistry.register(new guestbook.SetGuestBookEntries());
actionRegistry.register(new guestbook.SetVisibleEntries());

actionRegistry.register(new gigList.SetCurrentGigs());
actionRegistry.register(new gigList.SetNextGigs());
actionRegistry.register(new gigList.SetPreviousGigs());

//-------------------------------------------------------------------------------
export const store = createStore(
  (state: IAppState, action: ActionItem) =>
    state === undefined ? initState() : reduce(state, action));

function reduce(state: IAppState, action: ActionItem): IAppState {
  return actionRegistry.lookup(action.type).reduce(state, action);
}

//-------------------------------------------------------------------------------
guestbook
  .fetchEntries()
  .then(es => store.dispatch(guestbook.SetGuestBookEntries.emit(es)));

gigList
  .fetchGigs()
  .then(gs => {
    store.dispatch(gigList.SetCurrentGigs.emit(gs.current));
    store.dispatch(gigList.SetNextGigs.emit(gs.current));
    store.dispatch(gigList.SetPreviousGigs.emit(gs.previous));
  });
