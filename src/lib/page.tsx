export enum Page {
  Gigs,
  Impressions,
  About,
  Join,
  Contact,
  Legal,
  Guestbook,
  Press,
  Unspecified
}
