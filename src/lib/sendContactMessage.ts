import { EContactMailState } from './state';
import { setIn } from 'immutable';
import { ActionHandler } from './actions';

export const SendContactMessageRequest = ActionHandler<any>(
  'send-contact-message-request',
  (s) => setIn(s, ['ui', 'contactMail', 'state'], EContactMailState.Sending)
);

export const SendContactMessageSuccess = ActionHandler<any>(
  'send-contact-message-success',
  (s) => setIn(s, ['ui', 'contactMail', 'state'], EContactMailState.Success)
);

export const SendContactMessageFailed = ActionHandler<any>(
  'send-contact-message-failed',
  (s) => setIn(s, ['ui', 'contactMail', 'state'], EContactMailState.Failed)
);
