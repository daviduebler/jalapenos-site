export const Impressions = () => (
  <div>
    <h1>Eindrücke (in Arbeit…)</h1>
    <h2 style="text-align: center">
      Hier seht ihr bald eine Auswahl von Bildern und Videos von vergangenen Auftritten.
    </h2>
  </div>
);
