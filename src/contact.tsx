import { Component } from 'inferno';
import { connect } from 'inferno-redux';

/* import { EContactMailState } from './lib/state';
 * import { SendContactMessageRequest,
 *          SendContactMessageSuccess,
 *          SendContactMessageFailed } from './lib/sendContactMessage';*/
import { ENVIRONMENT } from './lib/environment';

import submit       from '../assets/wegschicken.png';
import submitActive from '../assets/wegschicken-active.png';
import './contact.scss';

enum MailState {
  Idle,
  Sending,
  Success,
  Failed,
}

class ContactForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      txtFrom: '',
      txtMessage: '',
      mailState: MailState.Idle,
    };
  }

  private setFrom(event) {
    this.setState({txtFrom: event.target.value});
  }

  private setMessage(event) {
    this.setState({txtMessage: event.target.value});
  }

  private async sendMessage() {
    this.setState({'mailState': MailState.Sending});

    try {
      await fetch(ENVIRONMENT.apiSendMessageUrl, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          'from': this.state.txtFrom,
          'message': this.state.txtMessage
        }),
      });

      this.setState({'mailState': MailState.Success});
    } catch(_) {
      this.setState({'mailState': MailState.Failed});
    }
  }

  public render({}, {mailState})
  {
    const contactForm = () => (
      <div class="contact-form">
        <input placeholder="Mailadresse / Telefonnummer"
               value={this.state.txtFrom}
               onInput={this.setFrom.bind(this)}
        />
        <textarea placeholder="Deine Nachricht"
                  value={this.state.txtMessage}
                  onInput={this.setMessage.bind(this)}
        />

        <div class="send-button"
             onClick={this.sendMessage.bind(this)}
        >
          <img src={submit}       class="inactive" />
          <img src={submitActive} class="active"   />
          <span>Abschicken</span>
        </div>
      </div>
    );

    const sending = () => (
      <div class="contact-feedback sending">
        <span>Deine Nachricht wird gesendet...</span>
      </div>
    );

    const success = () => (
      <div class="contact-feedback success">
        <span>Deine Nachricht ist angekommen!</span>
      </div>
    );

    const failed = () => (
      <div class="contact-feedback failed">
        <span>
          Deine Nachricht ist leider irgendwo verloren gegangen.
          Du kannst uns aber gerne eine Mail schreiben oder anrufen.
          Das klappt meistens.
        </span>
      </div>
    );

    switch (mailState) {
      case MailState.Sending: return sending();
      case MailState.Success: return success();
      case MailState.Failed: return failed();
      default: return contactForm();
    }
  }
}

export const Contact = () => (
  <div class="contact-wrapper">
    <h1>Kontakt zu uns</h1>

    <p>
      Ihr macht ein Event und sucht noch nach der gewissen Schärfe in eurem
      Programm? Ihr wollt in unser Training reinschnuppern oder gleich
      mitmachen? Ihr habt Verbesserungsvorschläge oder begeistertes Lob, das ihr
      uns mitteilen möchtet?
    </p>

    <p>
      Für eure Anliegen schreibt uns doch gerne eine Mail
      an <a href="mailto:jalapenos@afrocuban.eu">jalapenos@afrocuban.eu</a> oder
      nutzt das Kontaktformular. Ihr könnt uns zudem über unseren Leiter
      Andreas Bornholdt unter der Telefonnummer 0151/15357665 erreichen.
    </p>

    <ContactForm />
  </div>
);
