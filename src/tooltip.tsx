import { Component } from 'inferno';
import './tooltip.scss';

enum Position {
  Top,
  Bottom,
  Left,
  Right
}

interface Props {
  children?: any;
}

interface State {
  position: Position;
}

export class ToolTip extends Component<Props, State> {
  private ref: Element;
  private resizeListener: any;
  private failedAttempts: number = 0;
  private resetTimer: any | undefined = undefined;

  private static readonly MAX_ATTEMPTS = 4;
  private static readonly FAIL_TIMEOUT = 1000;

  private positionCandidates = [
    Position.Top,
    Position.Bottom,
    Position.Left,
    Position.Right
  ];

  private failedCandidates: Array<Position> = [];

  constructor() {
    super();
    this.state = {position: this.positionCandidates[0]};
  }

  public componentDidMount() {
    this.resizeListener = () => {
      if (this.failedAttempts < ToolTip.MAX_ATTEMPTS) {
        this.resetCandidates();
        this.updatePosition();
      } else {
        if (this.resetTimer === undefined) {
          this.resetTimer = setTimeout(() => {
            this.resetTimer = undefined;
            this.resetFailed();
          }, ToolTip.FAIL_TIMEOUT);
        }
      }
    };

    window.addEventListener('resize', this.resizeListener);
    this.resizeListener();
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', this.resizeListener);
  }

  public componentDidUpdate() {
    this.resizeListener();
  }

  private updatePosition() {
    const bounds = this.ref.getBoundingClientRect();
    const viewport = {
      width: document.documentElement.clientWidth,
      height: document.documentElement.clientHeight
    };

    if (bounds.left < 0 ||
        bounds.top < 0 ||
        bounds.right > viewport.width ||
        bounds.bottom > viewport.height)
    {
      const current = this.positionCandidates.shift();

      if (this.failedAttempts < ToolTip.MAX_ATTEMPTS) {
        this.failedCandidates.push(current);
        this.tryReposition();
        this.incFailed();
      }
    } else {
      this.failedAttempts = 0;
    }
  }

  private tryReposition() {
    if (this.positionCandidates.length > 0) {
      this.setState({position: this.positionCandidates[0]});
    }
  }

  private incFailed() {
    ++this.failedAttempts;
  }

  private resetFailed() {
    this.failedAttempts = 0;
    this.resetCandidates();
  }

  private resetCandidates() {
    this.positionCandidates =
      this.positionCandidates.concat(this.failedCandidates);

    this.failedCandidates = [];
  }

  private get positionClass() {
    switch (this.state.position) {
        case Position.Top    : return "top";
        case Position.Bottom : return "bottom";
        case Position.Left   : return "left";
        case Position.Right  : return "right";
    }
  }

  public render({children}: any) {
    const wrapperClasses = `tooltip ${this.positionClass}`;

    return (
      <div class={wrapperClasses} ref={ref => this.ref = ref}>
        <div class="tooltipcontent">
          {children}
        </div>
      </div>
    );
  }
}
