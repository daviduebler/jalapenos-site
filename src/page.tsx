import { Header } from './header';
import { Navbar } from './navbar';
import { Content } from './content';
import { Switch, BrowserRouter, Route } from 'inferno-router';
import { Intro } from './intro';

const MainPage = () => (
  <div>
    <Header/>
    <Navbar/>
    <Content/>
  </div>
);

export const Page = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Intro} />
      <Route component={MainPage} />
    </Switch>
  </BrowserRouter>
);
