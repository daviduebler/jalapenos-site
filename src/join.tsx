import { Link } from 'inferno-router';

const MRG_MAP_LINK =
  "https://www.openstreetmap.org/"
  + "?mlat=49.44242&mlon=11.85191#map=18/49.44242/11.85191";

export const Join = () => (
  <div>
    <h1>Mitmachen</h1>

    <p>
      Sobald irgendwo Musik erklingt springst du auf die Tanzfläche? Du kannst
      nicht anders, als im Zug gegen die Sitze zu klopfen und dir Rythmen
      durchzudenken? Du hast also Spaß an Tanz, Bewegung, Rythmus und Bass in
      der Magengrube? Dann bist du bei uns genau richtig!
    </p>

    <p>
      Bei uns muss man nicht Musik studiert haben, um mitzumachen – die
      Begeisterung für Bewegung, für Rhythmus und Tanz sind jedoch wichtige
      Grundlagen, um Spaß am Lernen all unserer neuen Stücke zu haben. Auch die
      Bereitschaft regelmäßig zu üben ist bei dem vielseitigen Programm, das wir
      uns mittlerweile aufgebaut haben unverzichtbar.
    </p>

    <h2>Training speziell für Einsteiger</h2>

    <p>
      Bei unseren Basistrainings bekommst du sofort ein Instrument an die Hand
      und kannst dich in den Rhythmus-Übungen langsam in die Grundlagen der
      Percussion-Musik einarbeiten.
    </p>

    <p>
      Bei der Instrumenten-Wahl kannst du dir die Zeit lassen, die du brauchst,
      um zu sehen, welches Instrument zu dir passt. Tambourims haben viel
      Choreographie, Surdos viel Bass – zu jedem passt ein anderes Instrument.
    </p>

    <p>
      Unsere tollen Leiter gehen in den Basistrainings gerne auf deine Fragen als
      Anfänger ein. Im Gesamttraining ist dann Abschauen, Ausprobieren und
      „Learning-by-doing“ angesagt. Aber keine Sorge, wir sind alle offen für
      neue begeisterte und engagierte MitspielerInnen und helfen gern weiter.
    </p>

    <p>
      <Link to="/contact">Schreib uns einfach an</Link> oder komm vorbei an einem
      unserer nächsten Trainings: Immer Donnerstags um 18:00 in
      der <a href={MRG_MAP_LINK} target="_blank">MRG Turnhalle</a>.
    </p>
  </div>
);
