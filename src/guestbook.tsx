import { Component } from 'inferno';
import { connect } from 'inferno-redux';
import { Route, Redirect } from 'inferno-router';
import { SetGuestBookEntries, SetVisibleEntries, fetchEntries,
         acceptEntry, submitEntry
       } from './lib/guestbook';
import submit       from '../assets/wegschicken.png';
import submitActive from '../assets/wegschicken-active.png';
import './guestbook.scss';

export enum SubmitState {
  Idle,
  Sending,
  Success,
  Failed,
}

const GuestbookEntry = (entry) => (
  <div class="entry">
    {entry.message.split('\n').map(l => <p>{l}</p>)}
    <span class="date">{entry.date}</span>
  </div>
);

class SubmitForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      txtMessage: '',
      submitState: SubmitState.Idle,
    };
  }

  private setMessage(event) {
    this.setState({txtMessage: event.target.value});
  }

  private submitEntry() {
    submitEntry(
      this.state.txtMessage,
      { 'sending': () => this.setState({'submitState': SubmitState.Sending}),
        'success': () => this.setState({'submitState': SubmitState.Success}),
        'failed' : () => this.setState({'submitState': SubmitState.Failed}),
      });
  }

  public render({}, {submitState})
  {

    const contactForm = () => (
      <div>
        <div class="submit-form">
          <textarea placeholder="Deine Nachricht"
                    value={this.state.txtMessage}
                    onInput={this.setMessage.bind(this)}
          />

          <div class="send-button"
              onClick={this.submitEntry.bind(this)}
          >
            <img src={submit}       class="inactive" />
            <img src={submitActive} class="active"   />
            <span>Abschicken</span>
          </div>
        </div>
        <p class="legal-notice">
          * Was du hier eintippst wird nach Prüfung unsererseits auf dieser
          Seite veröffentlicht.
        </p>
      </div>
    );

    const sending = () => (
      <div class="contact-feedback sending">
        <span>
          Deine Nachricht ist unterwegs...
        </span>
      </div>
    );

    const success = () => (
      <div class="contact-feedback success">
        <span>Danke für deine Rückmeldung</span>
      </div>
    );

    const failed = () => (
      <div class="contact-feedback failed">
        <span>
          Das hat leider nicht geklappt. Du kannst uns deine Rückmeldung aber
          auch per Mail schreiben.
        </span>
      </div>
    );

    switch (submitState) {
      case SubmitState.Sending: return sending();
      case SubmitState.Success: return success();
      case SubmitState.Failed: return failed();
      default: return contactForm();
    }
  }
}

const AcceptEntry = connect(
  state => {},
  dispatch => ({'setEntries': (es) => dispatch(SetGuestBookEntries.emit(es))}),
)(class extends Component {
  constructor(props) {
    super(props);
    this.state = {'patching': false, 'patchDone': false};
  }

  private patch() {
    const match = this.props.match;
    const id = match.params.id;
    const pin = match.params.pin;

    acceptEntry(id, pin).then(() => {
      fetchEntries().then(
        (es) => {
          this.props.setEntries(es);
          this.setState({'patchDone': true});
        }
      );
    });
  }

  public render() {
    if (! this.state.patching) {
      this.setState({'patching': true});
      this.patch();
    }

    return (
      this.state.patchDone
      ? <Redirect to="/guestbook" />
      : <div>Schalte Eintrag frei...</div>);
  }
});

export const Guestbook = connect(
  state => ({
    entries: state.getIn(["data", "guestbook", "guestBookEntries"]).toJS(),
    entriesVisible: state.getIn(["ui", "guestbook", "entriesVisible"]),
    entriesStep: state.getIn(["ui", "guestbook", "entriesStep"]),
    entriesCount: state.getIn(["data", "guestbook", "guestBookEntries"]).count(),
  }),
  dispatch => ({
    'setVisibleEntries': c => dispatch(SetVisibleEntries.emit(c)),
  })
)(({entries, entriesVisible, entriesStep, entriesCount, setVisibleEntries}) => (
  <div class="guestbook-wrapper">
    <h2>Gib gern deinen Senf dazu - wir freuen uns über Anregungen, Lob und Kritik*:</h2>
    <SubmitForm />

    <h2>Einträge</h2>
    <div class="entries">
      {entries.slice(0, entriesVisible).map(GuestbookEntry)}
    </div>

    {(entriesVisible < entriesCount)
     ? (<div class="show-more"
             onClick={() => setVisibleEntries(entriesVisible + entriesStep)}
        >
          ältere Einträge anzeigen
        </div>)
      : undefined}

    <Route path="/guestbook/accept/:id/:pin" component={AcceptEntry}/>
  </div>
));
