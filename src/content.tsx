import { Switch, Route } from 'inferno-router';
import { Component } from 'inferno';
import { connect } from 'inferno-redux';
import { PageSwapRequest, PageSwap } from './lib/pageSwap';
import { Page } from './lib/page';
import { Gigs } from './gigs';
import { Impressions } from './impressions';
import { About } from './about';
import { Join } from './join';
import { Contact } from './contact';
import { Guestbook } from './guestbook';
import { Press } from './press';
import { Legal, LegalFooter } from './legal';
import './content.scss';


const PageSwapTrigger = (page: Page) =>
  class extends Component {
    public componentWillMount() {
      (this as any).context.store.dispatch(PageSwapRequest.emit(page));
    }
  };

const PageRoute = connect(
  state => ({pageSelection: state.ui.pageSelection}),
  dispatch => ({commitPage: (page) => dispatch(PageSwap.emit(page))}),
)(({page, pageSelection, commitPage, children}) => {
  const isCurrent = pageSelection.currentPage === page;
  const isRequested = pageSelection.requestedPage === page;

  const wrapped = (animClass, onEnd) => (
    <div class={`pageWrapper ${animClass || ''}`} onAnimationEnd={onEnd}>
      {children}
    </div>
  );

  if (isCurrent) {
    return isRequested
         ? wrapped('fadeIn', () => {})
         : wrapped('fadeOut', () => commitPage(pageSelection.requestedPage));
  } else {
    return undefined;
  }
});

export const Content = () => (
  <div class="content-wrapper">
    <Switch>
      <Route path="/gigs"        component={PageSwapTrigger(Page.Gigs)}        />
      <Route path="/impressions" component={PageSwapTrigger(Page.Impressions)} />
      <Route path="/about"       component={PageSwapTrigger(Page.About)}       />
      <Route path="/join"        component={PageSwapTrigger(Page.Join)}        />
      <Route path="/contact"     component={PageSwapTrigger(Page.Contact)}     />
      <Route path="/guestbook"   component={PageSwapTrigger(Page.Guestbook)}   />
      <Route path="/press"       component={PageSwapTrigger(Page.Press)}       />
      <Route path="/legal"       component={PageSwapTrigger(Page.Legal)}       />
    </Switch>

    <div class="content">
      <PageRoute page={Page.Gigs}       ><Gigs        /><LegalFooter /></PageRoute>
      <PageRoute page={Page.Impressions}><Impressions /><LegalFooter /></PageRoute>
      <PageRoute page={Page.About}      ><About       /><LegalFooter /></PageRoute>
      <PageRoute page={Page.Join}       ><Join        /><LegalFooter /></PageRoute>
      <PageRoute page={Page.Contact}    ><Contact     /><LegalFooter /></PageRoute>
      <PageRoute page={Page.Guestbook}  ><Guestbook   /><LegalFooter /></PageRoute>
      <PageRoute page={Page.Press}      ><Press       /><LegalFooter /></PageRoute>
      <PageRoute page={Page.Legal}      ><Legal       /></PageRoute>
    </div>
  </div>
);
