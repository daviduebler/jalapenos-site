import { Link } from 'inferno-router';
import './legal.scss';

export const LegalFooter = () => (
  <div class="legal-footer">
    <Link to="/legal">Impressum | Datenschutz</Link>
  </div>
);

export const Legal = () => (
  <div>
    <h2>Angaben gemäß § 5 TMG</h2>
    <p>
      David Übler<br/>
      Lipowskystr. 3<br/>
      92224 Amberg<br/>
    </p>

    <h2>Haftung für Inhalte</h2>
    <p>
      Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die
      Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch
      keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG
      für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen
      verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch
      nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu
      überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige
      Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der
      Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon
      unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt
      der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden
      von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend
      entfernen.
    </p>

    <h2>Haftung für Links</h2>
    <p>
      Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren
      Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden
      Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten
      Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten
      verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung
      auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum
      Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche
      Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte
      einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von
      Rechtsverletzungen werden wir derartige Links umgehend entfernen.
    </p>

    <h2>Datenschutzerklärung</h2>
    <h2>1. Datenschutz auf einen Blick</h2>
    <h2>Allgemeine Hinweise</h2>
    <p>
      Die folgenden Hinweise geben einen einfachen Überblick darüber, was mit
      Ihren personenbezogenen Daten passiert, wenn Sie unsere Website besuchen.
      Personenbezogene Daten sind alle Daten, mit denen Sie persönlich
      identifiziert werden können. Ausführliche Informationen zum Thema
      Datenschutz entnehmen Sie unserer unter diesem Text aufgeführten
      Datenschutzerklärung.
    </p>

    <h2>Datenerfassung auf unserer Website</h2>
    <p>
      Wer ist verantwortlich für die Datenerfassung auf dieser Website? Die
      Datenverarbeitung auf dieser Website erfolgt durch den Websitebetreiber.
      Dessen Kontaktdaten können Sie dem Impressum dieser Website entnehmen. Wie
      erfassen wir Ihre Daten? Ihre Daten werden dadurch erhoben, dass Sie uns
      diese mitteilen. Hierbei kann es sich z.B. um Daten handeln, die Sie in
      ein Kontaktformular eingeben.
    </p>

    <h2>Welche Rechte haben Sie bezüglich Ihrer Daten?</h2>
    <p>
      Sie haben jederzeit das Recht unentgeltlich Auskunft über Herkunft,
      Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu
      erhalten. Sie haben außerdem ein Recht, die Berichtigung, Sperrung oder
      Löschung dieser Daten zu verlangen. Hierzu sowie zu weiteren Fragen zum
      Thema Datenschutz können Sie sich jederzeit unter der im Impressum
      angegebenen Adresse an uns wenden. Des Weiteren steht Ihnen ein
      Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Außerdem haben
      Sie das Recht, unter bestimmten Umständen die Einschränkung der
      Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Details hierzu
      entnehmen Sie der Datenschutzerklärung unter „Recht auf Einschränkung der
      Verarbeitung“.
    </p>

    <h2>2. Allgemeine Hinweise und Pflichtinformationen</h2>
    <p>
      Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten
      sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und
      entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser
      Datenschutzerklärung. Wenn Sie diese Website benutzen, werden verschiedene
      personenbezogene Daten erhoben. Personenbezogene Daten sind Daten, mit
      denen Sie persönlich identifiziert werden können. Die vorliegende
      Datenschutzerklärung erläutert, welche Daten wir erheben und wofür wir sie
      nutzen. Sie erläutert auch, wie und zu welchem Zweck das geschieht. Wir
      weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der
      Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein
      lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht
      möglich.
    </p>

    <h2>Hinweis zur verantwortlichen Stelle</h2>
    <p>
      Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website
      ist: David Übler, Lipowskystr. 3, 92224 Amberg. Telefon: 0176/ 56 72 63
      92. E-Mail: dsu@posteo.org. Verantwortliche Stelle ist die natürliche oder
      juristische Person, die allein oder gemeinsam mit anderen über die Zwecke
      und Mittel der Verarbeitung von personenbezogenen Daten (z.B. Namen,
      E-Mail-Adressen o. Ä.) entscheidet.
    </p>

    <h2>Widerruf Ihrer Einwilligung zur Datenverarbeitung</h2>
    <p>
      Viele Datenverarbeitungsvorgänge sind nur mit Ihrer ausdrücklichen
      Einwilligung möglich. Sie können eine bereits erteilte Einwilligung
      jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an
      uns. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung
      bleibt vom Widerruf unberührt.
    </p>

    <h2>Beschwerderecht bei der zuständigen Aufsichtsbehörde</h2>
    <p>
      Im Falle von Verstößen gegen die DSGVO steht den Betroffenen ein
      Beschwerderecht bei einer Aufsichtsbehörde, insbesondere in dem
      Mitgliedstaat ihres gewöhnlichen Aufenthalts, ihres Arbeitsplatzes oder
      des Orts des mutmaßlichen Verstoßes zu. Das Beschwerderecht besteht
      unbeschadet anderweitiger verwaltungsrechtlicher oder gerichtlicher
      Rechtsbehelfe.
    </p>

    <h2>SSL- bzw. TLS-Verschlüsselung</h2>
    <p>
      Diese Seite nutzt aus Sicherheitsgründen und zum Schutz der Übertragung
      vertraulicher Inhalte, wie zum Beispiel Bestellungen oder Anfragen, die
      Sie an uns als Seitenbetreiber senden, eine SSL-bzw. TLSVerschlüsselung.
      Eine verschlüsselte Verbindung erkennen Sie daran, dass die Adresszeile
      des Browsers von “http://” auf “https://” wechselt und an dem
      Schloss-Symbol in Ihrer Browserzeile. Wenn die SSL- bzw.
      TLS-Verschlüsselung aktiviert ist, können die Daten, die Sie an uns
      übermitteln, nicht von Dritten mitgelesen werden.
    </p>

    <h2>Auskunft, Sperrung, Löschung und Berichtigung</h2>
    <p>
      Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit das
      Recht auf unentgeltliche Auskunft über Ihre gespeicherten
      personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der
      Datenverarbeitung und ggf. ein Recht auf Berichtigung, Sperrung oder
      Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema
      personenbezogene Daten können Sie sich jederzeit unter der im Impressum
      angegebenen Adresse an uns wenden.
    </p>

    <h2>Recht auf Einschränkung der Verarbeitung</h2>
    <p>
      Sie haben das Recht, die Einschränkung der Verarbeitung Ihrer
      personenbezogenen Daten zu verlangen.

      Hierzu können Sie sich jederzeit unter der im Impressum angegebenen
      Adresse an uns wenden. Das Recht auf Einschränkung der Verarbeitung
      besteht in folgenden Fällen: Wenn Sie die Richtigkeit Ihrer bei uns
      gespeicherten personenbezogenen Daten bestreiten, benötigen wir in der
      Regel Zeit, um dies zu überprüfen. Für die Dauer der Prüfung haben Sie das
      Recht, die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu
      verlangen. Wenn die Verarbeitung Ihrer personenbezogenen Daten
      unrechtmäßig geschah / geschieht, können Sie statt der Löschung die
      Einschränkung der Datenverarbeitung verlangen. Wenn wir Ihre
      personenbezogenen Daten nicht mehr benötigen, Sie sie jedoch zur Ausübung,
      Verteidigung oder Geltendmachung von Rechtsansprüchen benötigen, haben Sie
      das Recht, statt der Löschung die Einschränkung der Verarbeitung Ihrer
      personenbezogenen Daten zu verlangen. Wenn Sie einen Widerspruch nach Art.
      21 Abs. 1 DSGVO eingelegt haben, muss eine Abwägung zwischen Ihren und
      unseren Interessen vorgenommen werden. Solange noch nicht feststeht,
      wessen Interessen überwiegen, haben Sie das Recht, die Einschränkung der
      Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Wenn Sie die
      Verarbeitung Ihrer personenbezogenen Daten eingeschränkt haben, dürfen
      diese Daten – von ihrer Speicherung abgesehen – nur mit Ihrer Einwilligung
      oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen
      oder zum Schutz der Rechte einer anderen natürlichen oder juristischen
      Person oder aus Gründen eines wichtigen öffentlichen Interesses der
      Europäischen Union oder eines Mitgliedstaats verarbeitet werden.
    </p>

    <h2>3. Datenerfassung auf unserer Website</h2>
    <p>
      Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre
      Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen
      Kontaktdaten zwecks Bearbeitung der Anfrage und für den Fall von
      Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht ohne Ihre
      Einwilligung weiter. Die Verarbeitung der in das Kontaktformular
      eingegebenen Daten erfolgt somit ausschließlich auf Grundlage Ihrer
      Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Sie können diese Einwilligung
      jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an
      uns. Die Rechtmäßigkeit der bis zum Widerruf erfolgten
      Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt. Die von Ihnen im
      Kontaktformular eingegebenen Daten verbleiben bei uns, bis Sie uns zur
      Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der
      Zweck für die Datenspeicherung entfällt (z.B. nach abgeschlossener
      Bearbeitung Ihrer Anfrage). Zwingende gesetzliche Bestimmungen –
      insbesondere Aufbewahrungsfristen – bleiben unberührt.
    </p>

    <p>Quelle: e-recht24.de</p>
  </div>
);
