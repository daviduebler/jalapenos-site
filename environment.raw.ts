export const ENVIRONMENT = {
  'apiUrl'                  : '${API_URL}',
  'apiSendMessageUrl'       : '${API_URL}/contact-messages',
  'apiFetchGuestbookEntries': '${API_URL}/guestbook/entries',
  'apiSubmitGuestbookEntry' : '${API_URL}/guestbook/entries',
  'apiAcceptGuestbookEntry' : (id) => '${API_URL}/guestbook/entries/' + id,
  'apiFetchGigs'            : '${API_URL}/gigs/entries',
};
