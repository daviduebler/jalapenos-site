#!/usr/bin/env bash

FILES=`find . ! -path "./node_modules*" ! -path "./.git/*"`
chown node:node $FILES
su - node -c sh -c "npm start"
