import smtplib
from os import environ
from email.message import EmailMessage

CONTACT_MAIL_ADDR_SEND = environ["CONTACT_MAIL_ADDR_SEND"]
CONTACT_MAIL_ADDR_RECV = environ["CONTACT_MAIL_ADDR_RECV"]
CONTACT_MAIL_HOST = environ["CONTACT_MAIL_HOST"]
CONTACT_MAIL_USER = environ["CONTACT_MAIL_USER"]
CONTACT_MAIL_PASS = environ["CONTACT_MAIL_PASS"]


def send(subject, message):
    with smtplib.SMTP_SSL(CONTACT_MAIL_HOST) as smtp:
        smtp.login(CONTACT_MAIL_USER, CONTACT_MAIL_PASS)

        msg = EmailMessage()
        msg.set_content(message)
        msg["Subject"] = subject
        msg["From"] = CONTACT_MAIL_ADDR_SEND
        msg["To"] = CONTACT_MAIL_ADDR_RECV

        smtp.send_message(msg)
