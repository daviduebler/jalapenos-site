import pickle

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = [
    'https://www.googleapis.com/auth/spreadsheets',
    'https://www.googleapis.com/auth/drive',
]

TOKEN_FILE = 'credentials/token.pickle'
CREDENTIALS_FILE = 'credentials/client_id.json'


def make_auth_token():
    creds = get_auth_token()

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                CREDENTIALS_FILE, SCOPES)
            creds = flow.run_local_server()

        with open(TOKEN_FILE, 'wb') as token:
            pickle.dump(creds, token)

    return creds


def get_auth_token():
    with open(TOKEN_FILE, 'rb') as f:
        return pickle.load(f)


if __name__ == '__main__':
    make_auth_token()
