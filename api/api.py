import json

from flask import Flask, request, Response
from flask_cors import CORS

import mail
import guestbook
import gigs

app = Flask(__name__)
CORS(app)


@app.route("/contact-messages", methods=["POST"])
def send_contact_message():
    message = request.get_json()

    mail.send("Kontaktformular",
              ("Nachricht per Kontaktformular von %s:\n"
               % message.get("from", "(unbekannt)")) +
              "\n" +
              message.get("message", "(keine Nachricht)") +
              "\n")

    return Response(json.dumps({"message": message}),
                    mimetype="application/json")


@app.route("/guestbook/entries", methods=["POST"])
def submit_guestbook_entry():
    req = request.get_json()
    entry = guestbook.add_entry(req["message"])

    mail.send("Gästebuch Eintrag",
              ("Ein neuer Gästebucheintrag wurde eingeschickt:\n\n" +
               entry["entry"]["message"] +
               "\n\n" +
               "Zum Freigeben folgenden Link aufrufen: \n" +
               entry["acceptUrl"]))

    return Response({"result": "submitted"},
                    mimetype="application/json")


@app.route("/guestbook/entries/<id>", methods=["PATCH"])
def patch_guestbook_entry(id):
    patch = request.get_json()
    accepted = guestbook.accept_entry(id, patch["pin"])

    return Response(json.dumps({"accepted": accepted}),
                    mimetype="application/json")


@app.route("/guestbook/entries", methods=["GET"])
def fetch_guestbook_entries():
    entries = guestbook.fetch_entries()

    return Response(json.dumps({"entries": list(reversed(entries))}),
                    mimetype="application/json")


@app.route("/gigs/entries", methods=["GET"])
def fetch_gig_entries():
    if gigs.cache_is_stale():
        gigs.reset_cache()

    entries = gigs.fetch_entries()
    last_updated = gigs.get_cache_age()

    return Response(json.dumps({
        'entries': entries,
        'last_updated': last_updated,
    }), mimetype="application/json")


@app.route("/gigs/entries", methods=["DELETE"])
def reset_gig_cache():
    try:
        gigs.reset_cache()
        return Response(json.dumps({"deleted": True}),
                        mimetype="application/json")
    except Exception:
        return Response(json.dumps({"deleted": False}),
                        mimetype="application/json")


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=8080)
