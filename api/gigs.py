import json
from googleapiclient.discovery import build
from os import environ, remove, stat
from datetime import datetime

import auth

GIGS_SHEET_ID = '1BNhhpsORUxxr57tKc-waFqnerT85cm1o4KNIYwMjKXU'
GIGS_DATA_FILE = environ["GIGS_DATA_FILE"]
DATE_SHEET_FORMAT_STRING = "%d.%m.%y"
DATE_EXPORT_FORMAT_STRING = "%Y-%m-%d"
MAX_CACHE_AGE = 60 * 5


def _get_service():
    service = build('sheets', 'v4', credentials=auth.get_auth_token())
    sheets = service.spreadsheets()
    return sheets


def _date_sheet_to_export(date):
    return (datetime
            .strptime(date, DATE_SHEET_FORMAT_STRING)
            .strftime(DATE_EXPORT_FORMAT_STRING))


def _fetch_gigs_current(sheets):
    range = 'Gigs!A:D'
    result = sheets.values().get(spreadsheetId=GIGS_SHEET_ID,
                                 range=range).execute()
    values = result.get('values')[1:]

    def format_gig(row):
        date, time, place, title = row

        return {
            'date': _date_sheet_to_export(date),
            'time': time,
            'place': place,
            'title': title
        }

    return list(map(format_gig, values))


def _fetch_gigs_previous(sheets):
    range = 'Archiv!A:D'
    result = sheets.values().get(spreadsheetId=GIGS_SHEET_ID,
                                 range=range).execute()
    values = result.get('values')[1:]

    def format_gig(row):
        date, place, title = row[0:3]
        comment = row[3] if len(row) > 3 else ''

        return {
            'date': _date_sheet_to_export(date),
            'place': place,
            'title': title,
            'comment': comment,
        }

    def not_empty(row):
        return row is not None and len(row) >= 3

    return list(
        map(format_gig,
            filter(not_empty,
                   values)))


def _fetch_gigs_spreadsheet():
    sheets = _get_service()

    return {
        'current': _fetch_gigs_current(sheets),
        'previous': _fetch_gigs_previous(sheets),
    }


def fetch_entries():
    try:
        with open(GIGS_DATA_FILE, 'r') as f:
            return json.load(f)
    except FileNotFoundError:
        gigs = _fetch_gigs_spreadsheet()

        with open(GIGS_DATA_FILE, 'w') as f:
            json.dump(gigs, f)

        return gigs


def reset_cache():
    try:
        remove(GIGS_DATA_FILE)
    except FileNotFoundError:
        pass


def get_cache_age():
    try:
        mtime = datetime.fromtimestamp(stat(GIGS_DATA_FILE).st_mtime)
        return int((datetime.now() - mtime).total_seconds())
    except FileNotFoundError:
        return None


def cache_is_stale():
    age = get_cache_age()
    return (age is None) or (age > MAX_CACHE_AGE)
