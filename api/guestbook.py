import json
import uuid
from os import environ
from datetime import datetime, timezone

GUESTBOOK_DATA_FILE = environ["GUESTBOOK_DATA_FILE"]
PUBLIC_SITE_URL = environ["PUBLIC_SITE_URL"]
DATE_INTERNAL_FORMAT_STRING = "%Y-%m-%d %H:%M:%S %Z"


def _try_read_gb_data():
    try:
        with open(GUESTBOOK_DATA_FILE, "r") as f:
            return json.load(f)
    except FileNotFoundError:
        return []


def _write_gb_data(data):
    with open(GUESTBOOK_DATA_FILE, "w") as f:
        json.dump(data, f, indent=2)


def _append_gb_item(data, item):
    return data + [item]


def _find_gb_item(data, id):
    return next(filter(lambda el: el["id"] == id, data), None)


def _patch_gb_item(data, id, fields):
    item = _find_gb_item(data, id)

    for key, value in fields.items():
        item[key] = value

    return data


def add_entry(message):
    def random_id():
        return str(uuid.uuid4())

    item = {
        "id": random_id(),
        "date": (datetime
                 .now(timezone.utc)
                 .strftime(DATE_INTERNAL_FORMAT_STRING)),
        "message": message,
        "pin": random_id(),
        "accepted": False,
    }

    _write_gb_data(
        _append_gb_item(
            _try_read_gb_data(), item))

    return {
        "entry": item,
        "acceptUrl": "%s/guestbook/accept/%s/%s" % (
            PUBLIC_SITE_URL,
            item['id'],
            item['pin']
        ),
    }


def accept_entry(id, pin):
    data = _try_read_gb_data()
    entry = _find_gb_item(data, id)

    if entry and entry["pin"] == pin:
        _write_gb_data(
            _patch_gb_item(
                data, id, {"accepted": True}))

        return True

    return False


def fetch_entries():
    def format_entry(e):
        date = datetime.strptime(e["date"], DATE_INTERNAL_FORMAT_STRING)

        return {
            "date": date.strftime("%d.%m.%Y"),
            "message": e["message"],
        }

    return list(map(format_entry,
                    filter(lambda e: e["accepted"],
                           _try_read_gb_data())))
